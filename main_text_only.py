#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
from archivio_stanze import Dizionario
from map_generator import Dungeon
import random
import time


def main():
    print " --------------------------------------------- "
    print "| Dungeon Generator for The Gate Mud          |"
    print " --------------------------------------------- "
    ## Variabili di carattere generale. ##
    name = "dungeon"
    dungeon = []

    ## Configurazione del Dungeon. ##
    dimensione_livelli = 0
    profondita_dungeon = 0
    difficolta_incontri = 0
    
    ## Imposto la Dimensione dei Livelli. ##
    if(dimensione_livelli == 0):
        mud_rooms_x = 15
        mud_rooms_y = 15
        max_num_struc = 15
        min_num_rooms = 2
        min_room_size = (2, 2)
        max_room_size = (3, 3)
        min_corr_size = 2
        max_corr_size = 3
        # Imposto la dimensione di visualizzazine di una tile.
        tile_w = 10
        tile_h = 10
    elif(dimensione_livelli == 1):
        mud_rooms_x = 15
        mud_rooms_y = 15
        max_num_struc = 30
        min_num_rooms = 4
        min_room_size = (2, 2)
        max_room_size = (4, 4)
        min_corr_size = 2
        max_corr_size = 3
        # Imposto la dimensione di visualizzazine di una tile.
        tile_w = 10
        tile_h = 10    
    elif(dimensione_livelli == 2):
        mud_rooms_x = 30
        mud_rooms_y = 30
        max_num_struc = 60
        min_num_rooms = 6
        min_room_size = (3, 3)
        max_room_size = (5, 5)
        min_corr_size = 3
        max_corr_size = 5
        # Imposto la dimensione di visualizzazine di una tile.
        tile_w = 10
        tile_h = 10
    
    ## Imposto la Profondità del Dungeon. ##
    if(profondita_dungeon == 0):
        levels = random.randint(1, 3)
    elif(profondita_dungeon == 1):
        levels = random.randint(2, 5)
    elif(profondita_dungeon == 2):
        levels = random.randint(3, 7)
        
    ## Eseguo per un numero arbitrario di volte la generazione del dungeon.
    print "  - Starting generating..."
    start = time.time()
    for contatore in range(1, 100):
        ## Scelgo la tipologia di dungeon. ##
        tipi_dungeon = Dizionario.getDungeon_Types()
        scelta = random.randint(0, len(tipi_dungeon) - 1)
        tipo_dungeon = tipi_dungeon[scelta]
        
        ## Genero un dungeon iniziale. ##
        for livello in range(1, levels + 1):
            dungeon_level = Dungeon((mud_rooms_x, mud_rooms_y), name, livello, tipo_dungeon, max_num_struc, min_num_rooms, min_room_size, max_room_size, min_corr_size, max_corr_size, (tile_w, tile_h))
            dungeon_level.generate_dungeon()
            dungeon.append(dungeon_level)
        # Genero il file stanze.
        save = open("stanze." + name, "w")
        for dungeon_temp in dungeon:
            save.write(dungeon_temp.print_rooms_for_mud())
        save.close()

        # Genero i file zona e mobs.
        save = open("zona", "w")
        save.write("Nome \"" + name + "\"\n")
        save.write("Area \"" + name + "_prova\"\n")
        save.write("Evento iniz\n")
        save.write("    Esegui proc($zona)\n")
        save.close()
        for livello in range(0, len(dungeon)):
            save = open("mobs." + name + "_level_" + str(livello), "w")
            save.write(dungeon[livello].print_mobs_for_mud(difficolta_incontri, levels))
            save.close()
        save = open("zona", "a")
        save.write("    fine\n")
        save.write("fineevento\n")
        save.close()
        
        middle = time.time()
        print "    - Intermediate time : " + str(round(middle - start, 2)) + " seconds"
    end = time.time()
    print "  - Dungeons generated in " + str(round(end - start, 2)) + " seconds"
        
    
if __name__ == "__main__":
    main()
