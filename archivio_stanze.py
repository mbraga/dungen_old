#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
class Dizionario:
    """
    Questo dizionario permette di generare delle rozze descrizioni di stanze.
    """
    @staticmethod
    def getDungeon_Types():
        """
        Ritorna i tipi di dungeon generabili.
        """
        return ["Caverna", "Underdark" ]

    @staticmethod
    def getRoom_Rooms(zone, dimension):
        """
        Ritorna i possibili nomi di stanze per una tipologia di dungeon.
        """
        words = []
        if(zone == "Caverna"):
            if(dimension <= 3):
                words.append(["Un'angusta caverna", "a"])
                words.append(["Una piccola caverna", "a"])
            elif(3 < dimension <= 6):
                words.append(["Una grotta", "a"])
            elif(6 < dimension):
                words.append(["Una caverna naturale", "a"])
        elif(zone == "Underdark"):
            if(dimension <= 3):
                words.append(["Un antro oscuro", "o"])
                words.append(["Una cavita'", "a"])
            elif(3 < dimension <= 6):
                words.append(["Uno largo spiazzo", "o"])
            elif(6 < dimension):
                words.append(["Una oscura caverna", "a"])
        return words
        
    @staticmethod
    def getRoom_Corridors(zone, dimension):
        """
        Ritorna i possibili nomi di corridoi per una tipologia di dungeon.
        """
        words = []
        if(zone == "Caverna"):
            if(dimension <= 3):
                words.append(["Un cunicolo", "o"])
            elif(3 < dimension):
                words.append(["Un passaggio", "o"])
        elif(zone == "Underdark"):
            if(dimension <= 3):
                words.append(["Un buio cunicolo", "o"])
            elif(3 < dimension):
                words.append(["Un passaggio", "o"])
        return words
        
    @staticmethod
    def getRoom_Status(zone, gender):
        """
        Ritorna i possibili status in cui si puo' trovare una stanza.
        """
        words = []
        words.append([".", "normale"])
        if(zone == "Caverna"):
            words.append([" ghiacciat" + gender + "."                 , "ghiacciato"])
            words.append([" percors" + gender + " dalla lava."        , "lavico"])
            words.append([" infestat" + gender + " dalla vegetazione.", "vegetale"])
        elif(zone == "Underdark"):
            words.append([" ghiacciat" + gender + "."                 , "ghiacciato"])
            words.append([" percors" + gender + " dalla lava."        , "lavico"])
            words.append([" infestat" + gender + " dalla vegetazione.", "vegetale"])
        return words

    @staticmethod
    def getRoom_Descriptions_Begin(zone, room_type, gender):
        """
        Ritorna le possibili descrizioni genriche per un dato tipo di dungeon.
        """
        descriptions = []
        if(zone == "Caverna"):
            descriptions.append("Sei appena entrato in " + room_type.lower() + " senti il distante rumore di qualche animale, forse il suo letargo e' finito.")
            descriptions.append("Ti trovi in " + room_type.lower() + " in alcuni punti le pareti presentano venature metalliche.")
            descriptions.append("I tuoi passi risultano amplificati mentre avanzi nell'oscurita' ed essi, oltre al suono del tuo respiro, sono gli unici suoni che riesci a percepire.")
            descriptions.append("Appena sbuchi da un crepaccio ti guardi attorno, ora ti trovi in " + room_type.lower() + ".")
        elif(zone == "Underdark"):
            descriptions.append("Ti guardi attorno, ma come ci sei finito qui non lo sai nemmeno tu, ne io. Insomma ti trovi in " + room_type.lower() + " e ora meglio se pensi a dove andare.")
            descriptions.append("L'urlo sovrumano di qualche animale proviene da un piccolo scrano a pochi passi da te. Ti volti, ma nulla si para d'innanzi ai tuoi occhi, forse ora e' alle tue spalle.")
            descriptions.append("I tuoi sensi ti avvertono di un pericolo, guardi di lato e vedi una parete di roccia ricoperta di fessure, agguzzi la vista e dentro noti tanti piccoli occhi.")
            descriptions.append("Entri in " + room_type.lower() + " e l'oscurita' ti inghiotte. Fai qualche passo e nulla cambia, anzi, all'oscurita' si aggiunge il silenzio tombale.")
        return descriptions
            
    @staticmethod
    def getRoom_Descriptions_Status(status):
        """
        Ritorna una serie di descrizioni supplementari basate sullo status della stanza del dungeon.
        """
        descriptions = []
        if(status == "normale"):
            descriptions.append("Non noti nulla di particolare qui.")
            descriptions.append("Ti senti osservato, ci mancava solo questo.")
            descriptions.append("Il tuo viaggio prosegue indisturbato, fosse sempre cosi'!")
        elif(status == "lavico"):
            descriptions.append("Una cascata di lava illumina leggermente la stanza, non che questo ti possa aiutare a vedere, di pero' il calore qui rende faticoso proseguire.")
            descriptions.append("Un fiume di lava passa sotto i tuoi piedi mentre avanzi su un ponte naturale, speriamo regga.")
        elif(status == "vegetale"):
            descriptions.append("Il pavimento e' interamente ricoperto di viticci che si intrecciano fra loro, rendendoti difficoltoso proseguire.")
            descriptions.append("Piante sorgono da crepe sul pavimento, allungate e rinsecchite sempre alla ricerca di un po di luce. Non credevi una pianta potesse fare tutto questo.")
        elif(status == "ghiacciato"):
            descriptions.append("La temperatura e' calata bruscamente, il freddo ti raggela il sangue che comincia a scorrere a fatica nelle tue vene.")
            descriptions.append("Una patina di brina ricopre tutto, l'aria stessa e' permeata da una coltre di finissima neve che a causa di piccoli fiotti di vento non ha intenzione di scendere verso il terreno.")
            descriptions.append("Stai camminando su una piccola lastra di ghiaccio, quando ecco che si crepa facendoti cadere in una piccola pozza d'acqua gelida, non potrebbe andare peggio di cosi'.")
        elif(status == "vecchio"):
            descriptions.append("L'aria qui e' quasi irrespirabile a causa della polvere che regna sovrana in un continuo fluttuare.")
            descriptions.append("Fa un passo e posi il piede su qualcosa di marcio, *crak*.. per poco non ci rimettevi una caviglia, meglio fare attenzione.")
        elif(status == "bruciato"):
            descriptions.append("Il fetore di bruciato e carbone permea l'aria, tenti di respirare a pieni polmoni ma riesci solo a peggiorare la tua situazione.")
            descriptions.append("Mucchietti di carbone sono tutto cio' che rimane di qualche antico mobilio.")
            descriptions.append("Cerchi di aprire una porta per avanzare, ma non appena tocchi il pomo tutta la struttura crolla lasciandoti in mano quel piccolo moncone, che ora apre.. niente.")
        return descriptions
