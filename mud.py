#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
from archivio_mob import Razze, NomiSoprannomiBoss, Modificatori, Equipaggiamento, Comportamenti
from archivio_stanze import Dizionario
import random
import subprocess

class MudExit:
    """
    Permette di definire l'uscita di una stanza.
    """
    exit_names = ["nord", "est", "sud", "ovest", "alto", "basso"]

    def __init__(self, direction, destination, name, description):
        if type(direction) is str:
            self.direction = self.name_to_num(direction)
        else:
            self.direction = direction
        self.destination = destination
        self.name = name
        self.description = description
        self.flasg = ""

    def print_for_mud(self):
        """
        Stampa la descrizione dell'uscita adatta per TheGate.
        """
        text = "Uscita\t" + self.num_to_name(self.direction) + " " + self.generate_flags() + " 0 <<s:" + self.destination + ">> 0\n"
        text += self.name + "\n"
        text += self.description + "\n"
        text += "\n"
        return text

    @staticmethod
    def generate_flags():
        """
        Ritorna la stringa contenente le flags da applicare ad una porta.
        """
        probabilita_porta = 25
        probabilita_porta_chiusa = 50
        probabilita_porta_bloccata = 30
        probabilita_porta_segreta = 20

        result = random.randint(1, 100)
        if(result <= probabilita_porta):
            flags = "(nomob | porta"
            result = random.randint(1, 100)
            if(result <= probabilita_porta_segreta):
                flags += " | segreta)"
            elif(result <= probabilita_porta_bloccata):
                flags += " | bloccata)"
            elif(result <= probabilita_porta_chiusa):
                flags += " | chiusa)"
            else:
                flags += ")"
        else:
            flags = "(nomob)"
        return flags

    @staticmethod
    def num_to_name(dir_num):
        """
        Ritorna la versione testuale della direzione di un'uscita.
        """
        return MudExit.exit_names[dir_num]

    @staticmethod
    def name_to_num(dir_str):
        """
        Ritorna la versione numerica della direzione di un'uscita.
        """
        return MudExit.exit_names.index(dir_str)

class MudRoom:
    """
    Permette di definire una stanza.
    """
    def __init__(self, id, name, description, terrain, has_chest):
        self.id = id
        if name == None:
            self.name = self.id
        else:
            self.name = name
        if description == None:
            self.description = self.id
        else:
            self.description = description
        if terrain == None:
            self.terrain = "interno"
        else:
            self.terrain = terrain
        if has_chest == None:
            self.has_chest = False
        else:
            self.has_chest = has_chest

        self.exits = [None] * 6
        

    def add_exit(self, e):
        """
        Aggiunge un'uscita alla stanza, il funzionamento è semplice:
            Una stanza ha associato un array lungo quanto le possibili uscite,
            in posizione 0 ad esempio c'e' l'uscita che va a nord.
            Utilizzando la variabile direction(numerica) dell'uscita, posizioniamo la medesima nell'array.
        """
        self.exits[e.direction] = e

    def print_for_mud(self):
        """
        Stampa una stanza adatta per TheGate.
        """
        text = "Stanza\t\"" + self.id + "\"\n"
        text += "Nome\t\"" + self.name + "\"\n"
        text += "Desc\n\"" + self.description + "\"\n"
        text += "Terr\t" + self.terrain + "\n\n"
        
        for e in self.exits:
            if e:
                text += e.print_for_mud()
                
        if(self.has_chest):
            text += "evento iniz\n"
            text += "    esegui proc($stanza)\n"
            text += "        $oggetto = carica_ogg(<<o:cassa_antica_dungeon>>)\n"
            text += "        muovi($oggetto, $stanza)\n"
            text += "    fine\n"
            text += "fineevento\n"

        text += "Finest\n\n"

        return text

class MudMob:
    """
    Permette di definire un mob.
    """
    def __init__(self, mob_id, proper_noun, names, short_desc, static_desc, desc, race, actions, flags, genabil, carat, combat, damage, gender, reaction, icon, evt_iniz, evt_aggressivo, evt_combat, evt_random):
        self.mob_id = mob_id
        if proper_noun == None or names == None or short_desc == None or static_desc == None or desc == None or race == None or gender == None:
            self.proper_noun = "Un ombra"
            self.names = ["ombra", "chupacabra"]
            self.short_desc = "un ombra"
            self.static_desc = "Un ombra è qui."
            self.desc = "Qualcosa non va, un'ombra non dovrebbe muoversi."
            self.race = "spe"
            self.gender = "neutro"
        else:
            self.proper_noun = proper_noun
            self.names = names
            self.short_desc = short_desc
            self.static_desc = static_desc
            self.desc = desc
            self.race = race
            self.gender = gender

        if actions == None:
            self.actions = ["sent", "pacif", "noparla"]
        else:
            self.actions = actions

        if flags == None:
            self.flags = ["nonviol"]
        else:
            self.flags = flags

        if genabil == None:
            self.genabil = 100
        else:
            self.genabil = genabil

        if carat == None:
            self.carat = [-100, -100, -100, -100, -100, -100, -100, -100, -100]
        else:
            self.carat = carat

        if combat == None:
            self.combat = [100, 100, 100]
        else:
            self.combat = combat

        if damage == None:
            self.damage = [10, 15]
        else:
            self.damage = damage

        if reaction == None:
            self.reaction = "ignora ignora ignora ignora ignora ignora ignora"
        else:
            self.reaction = reaction

        if icon == None:
            self.icon = 416
        else:
            self.icon = icon

        self.evt_iniz = evt_iniz
        self.evt_aggressivo = evt_aggressivo
        self.evt_combat = evt_combat
        self.evt_random = evt_random

    def print_for_mud(self, zone_name):
        """
        Stampa il mob sottoforma di stringa adatta per TheGate.
        """
        mob = "Mobile  \t\"" + self.mob_id + "\"\n"
        mob += "NomePr  \t\"" + self.proper_noun + "\"\n"
        mob += "Nome    \t{"
        for i in range (0, len(self.names)):
            if(i < len(self.names) - 1):
                mob += "\"" + self.names[i] + "\", "
            else:
                mob += "\"" + self.names[i] + "\"}\n"

        mob += "Descbr  \t\"" + self.short_desc + "\"\n"
        mob += "Descst  \t\"" + self.static_desc + "\"\n"
        mob += "Desc    \t\"" + self.desc + "\"\n"
        mob += "Razza   \t\"" + self.race + "\"\n"
        mob += "Azioni  \t("
        if(len(self.actions) > 1):
            for i in range (0, len(self.actions)):
                if(i < len(self.actions) - 1):
                    mob += self.actions[i] + " | "
                else:
                    mob += self.actions[i] + ")\n"
        else:
            mob += self.actions[0] + ")\n"

        mob += "Flags   \t("
        if(len(self.flags) > 1):
            for i in range (0, len(self.flags)):
                if(i < len(self.flags) - 1):
                    mob += self.flags[i] + " | "
                else:
                    mob += self.flags[i] + ")\n"
        else:
            mob += self.flags[0] + ")\n"

        mob += "GenAbil \t" + str(self.genabil) + "\n"
        mob += "Caratt  \t"
        for i in range (0, len(self.carat)):
            if(i < len(self.carat) - 1):
                mob += str(self.carat[i]) + " "
            else:
                mob += str(self.carat[i]) + "\n"

        mob += "Combat  \t" + str(self.combat[0]) + " " + str(self.combat[1]) + " " + str(self.combat[2]) + "\n"
        mob += "Danni   \t" + str(self.damage[0]) + " " + str(self.damage[0]) + "\n"
        mob += "Sesso   \t" + self.gender + "\n"
        mob += "Reaz    \t" + self.reaction + "\n"
        mob += "Icona   \t" + str(self.icon) + "\n\n"
        
        mob += self.evt_iniz
        mob += self.evt_aggressivo
        mob += self.evt_combat
        mob += self.evt_random
        
        # Stampo su file zona, il mob appena creato.
        save = open("zona", "a")
        save.write("        $mob = carica_mob(<<m:" + self.mob_id + "@" + zone_name + ">>)\n")
        save.write("        muovi($mob, $stanza)\n")
        save.write("        modi_repop($mob, $stanza, 800)\n")
        save.close()

        return mob

class MudGenerator:
    """
    Classe utilizzata per la generazione di stanze et mob.
    """
    @staticmethod
    def generate_room_name(room, dungeon_zone):
        """
        Genera un nome appropiato per la stanza in base al tipo di dungeon.
        """
        if (room.tipo == "ROOM"):
            dizionario = Dizionario.getRoom_Rooms(dungeon_zone, max(room.size[0], room.size[1]))
        else:
            dizionario = Dizionario.getRoom_Corridors(dungeon_zone, max(room.size[0], room.size[1]))
        
        # Scelgo la tipologia di stanza.
        scelta1 = random.randint(0, len(dizionario) - 1)
        name = dizionario[scelta1][0]
        
        # Scelgo lo stato della stanza.
        probabilita = random.randint(1, 100)
        if(probabilita > 60):
            status = Dizionario.getRoom_Status(dungeon_zone, dizionario[scelta1][1])
            scelta2 = random.randint(0, len(status) - 1)
            name += status[scelta2][0]
            return [name, dizionario[scelta1][0], dizionario[scelta1][1], status[scelta2][1]]
        else:
            name += "."
            return [name, dizionario[scelta1][0], dizionario[scelta1][1], "normale"]

    @staticmethod
    def generate_room_description(dungeon_zone, room_type, room_gender, room_status):
        """
        Genera una descrizione appropiata per la stanza.
        """
        partOne = Dizionario.getRoom_Descriptions_Begin(dungeon_zone , room_type  , room_gender)
        scelta1 = random.randint(0, len(partOne) - 1)
        partTwo = Dizionario.getRoom_Descriptions_Status(room_status)
        scelta2 = random.randint(0, len(partTwo) - 1)
        
        return partOne[scelta1] + " " + partTwo[scelta2]

    @staticmethod
    def generate_mob_group(dungeon_zone, dungeon_status, group_id, conformazione_gruppo):
        """
        Genera un gruppo di mob di dimensione variabile.
        """
        mob_group = []
        
        # Seleziono la razza del gruppo di nemici.
        var = Razze.getRazze_Zona_Status(Razze.generateRazze(), dungeon_zone, dungeon_status)
        scelta = random.randint(0, len(var) - 1)
        razza = var[scelta]
        
        # Genero tutti i mob che compongono il gruppo d'incontro.
        for i in range(0, len(conformazione_gruppo)):
            names = []
            carat = [-100, -100, -100, -100, -100, -100, -100, -100, -100]
            ##  RACE and GENDER ##
            if(random.randint(0, 1) == 0):
                sesso = "masch"
            else:
                sesso = "femmina"
                
            ## ID ##
            # L'id cambia in base al tipo di mob.
            if(conformazione_gruppo[i].startswith('M')):
                mob_id = group_id + "_mob_" + str(i + 1)
            else:
                mob_id = group_id + "_boss"

            ## PROPER_NOUN ##
            if(conformazione_gruppo[i].startswith('B')):
                nome = random.choice(NomiSoprannomiBoss.nomi_boss(razza.getTipologia(), sesso))
                soprannome = random.choice(NomiSoprannomiBoss.nomi_boss(razza.getTipologia(), sesso))
                proper_noun = nome + " " + soprannome
                names.append(nome.lower())
                names.append(soprannome.lower())
            else:
                proper_noun = razza.getNomeArticolato(sesso)

            ## NAMES ##
            nomi = razza.getNome().split(" ")
            for j in range(0, len(nomi)):
                names.append(nomi[j].lower())

            ## SHORT_DESC ##
            short_desc = proper_noun.lower()

            ## STATIC_DESC ##
            static_desc = proper_noun + " è quì."
            
            ## DESC ##
            desc = random.choice(["Vedi davanti a te ", "Ti si para davanti ", "Incroci lo sguardo di "]) + razza.getNomeArticolato(sesso).lower() + ". "
            if(razza.getTipologia() == 0):
                desc += subprocess.check_output(["gendesc/polygen", "gendesc/generatoreCorpo"])
            
            
            ## RACE ##
            race = razza.getRazza()

            ## ACTIONS ##
            actions = None

            ## FLAGS ##
            flags = None

            ## GENABIL ##
            genabil = None

            ## CARAT ##
            carat_base = razza.getCarat()
            # Imposto le caratteristiche in base al Grado Di Sfida del mob.
            carat_modi = Modificatori.applicaGradoSfida(conformazione_gruppo[i])
            for j in range(0, 9):
                carat[j] = carat_base[j] + carat_modi[j]

            ## COMBAT ##
            combat = None

            ## DAMAGE ##
            damage = None

            ## GENDER ##
            if(sesso == 0):
                gender = "masch"
            else:
                gender = "femmina"

            ## REACTION ##
            reaction = None

            ## ICON ##
            icon = None

            ## EVT_INIZ ##
            evt_iniz = ""
            if(razza.getTipologia() == 0):  # Se si tratta di un umanoide.
                if(len(conformazione_gruppo) > 1):
                    if(i == 0):
                        evt_iniz = Equipaggiamento.eq_capo(razza.getRazza())
                    else:
                        evt_iniz = Equipaggiamento.eq_seguace(razza.getRazza())
                else:
                    evt_iniz = Equipaggiamento.eq_boss(razza.getRazza())
            ## EVT_COMBAT ##
            if(len(conformazione_gruppo) > 1):
                if(i == 0):
                    evt_combat = Comportamenti.combattimento_capo([razza.getRazza()])
                else:
                    evt_combat = Comportamenti.combattimento_seguace([razza.getRazza()])
            else:
                evt_combat = Comportamenti.combattimento_boss([razza.getRazza()])
                
            ## EVT_RANDOM ##
            evt_random = Comportamenti.casuale()
            
            ## EVT_AGGRESSIVO ##
            if(len(conformazione_gruppo) > 1):
                if(i == 0):
                    evt_aggressivo = Comportamenti.aggressivo_capo([razza.getRazza()])
                else:
                    evt_aggressivo = Comportamenti.aggressivo_seguace([razza.getRazza()])
            else:
                evt_aggressivo = Comportamenti.aggressivo_boss([razza.getRazza()])



            ## Genero quindi il mob e lo aggiungo alla lista dei mob. ##
            mob_group.append(MudMob(mob_id, proper_noun, names, short_desc, static_desc, desc, race, actions, flags, genabil, carat, combat, damage, gender, reaction, icon, evt_iniz, evt_aggressivo, evt_combat, evt_random))
        return mob_group
