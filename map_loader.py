###########################################
### Dungeon Generator --- map_loader.py ###
###########################################
####### Breiny Games (c) 2011 #############
###########################################
## This file contains the classes and    ##
## functions used to graphically display ##
## the data representing a randomly      ##
## generated dungeon generated from the  ##
## map_generator.py module.              ##
###########################################

import pygame

class Tile:
    """
    Questa classe contiene le informazioni di una casella.
    """

    def __init__(self, x, y, w, h, tile_id):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.rect = pygame.Rect(self.x, self.y, self.w, self.h)
        self.image = pygame.surface.Surface((self.w, self.h))
        self.id = tile_id
        
        if self.id == 0:
            self.image.fill((54, 47, 45))
        elif self.id == 1:
            self.image.fill((166, 124, 82))
        elif self.id == 2 or self.id == 3 or self.id == 4 or self.id == 5 or self.id == 6:
            self.image.fill((96, 57, 19))
        elif self.id == 7:
            self.image.fill((0, 118, 163))
        elif self.id == 8:
            self.image.fill((75, 255, 75))
        elif self.id == 9:
            self.image.fill((255, 10, 10))
        elif self.id == 10:
            self.image.fill((255, 255, 100))
        elif self.id == 11:
            self.image.fill((255, 255, 255))
        elif self.id == 12:
            self.image.fill((126, 84, 42))

class Map:
    """
    Questa classe contiene la matrice di caselle utilizzate da pygame per disegnare il dungeon.
    """

    def __init__(self):
        self.dungeon = None
        self.tiles = []

    def load_dungeon(self, dungeon):
        self.dungeon = dungeon
        self.tiles = []
        for y in range(0, self.dungeon.grid_size[1]):
            row = []
            for x in range(0, self.dungeon.grid_size[0]):
                row.append(Tile(x * dungeon.tile_w,
                                y * dungeon.tile_h,
                                dungeon.tile_w,
                                dungeon.tile_h,
                                self.dungeon.grid[y][x]))
            self.tiles.append(row)

    def draw(self, surface):
        for row in self.tiles:
            for tile in row:
                surface.blit(tile.image, (tile.x, tile.y))
