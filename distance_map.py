import sys
def distance_map(mappa, map_goal_numbers, map_ignore_numbers):

    distance_map = []
    change = []
    
    # Cycle on each map grid element and create a distance map
    for y in range(0, len(mappa)):
        row = []
        for x in range(0, len(mappa[0])):
            # Set distance as -1 for all ignored squares
            # Set distance as 10 for each goal
            # For all other squares, 
            if mappa[y][x]   in map_ignore_numbers:
                row.append(-1)
            elif mappa[y][x] in map_goal_numbers:
                row.append(10)
            else:
                # row.append(int(len(mappa)+len(mappa[0])+10)/3)
                row.append(10 + len(mappa) + len(mappa[0]))
        distance_map.append(row)

    while 1:
        for y in range(0, len(distance_map)):
            for x in range(0, len(distance_map[0])):
                neighbor_values = []
                if x + 1 < len(distance_map[0]):
                    if distance_map[y][x + 1] != -1:
                        neighbor_values.append(distance_map[y][x + 1])
                if x - 1 >= 0:
                    if distance_map[y][x - 1] != -1:
                        neighbor_values.append(distance_map[y][x - 1])
                if y + 1 < len(distance_map):
                    if distance_map[y + 1][x] != -1:
                        neighbor_values.append(distance_map[y + 1][x])
                if y - 1 >= 0:
                    if distance_map[y - 1][x] != -1:
                        neighbor_values.append(distance_map[y - 1][x])
                if y - 1 >= 0 and x - 1 >= 0:
                    if distance_map[y - 1][x - 1] != -1:
                        neighbor_values.append(distance_map[y - 1][x - 1])
                if y - 1 >= 0 and x + 1 < len(distance_map[0]):
                    if distance_map[y - 1][x + 1] != -1:
                        neighbor_values.append(distance_map[y - 1][x + 1])
                if y + 1 < len(distance_map) and x - 1 >= 0:
                    if distance_map[y + 1][x - 1] != -1:
                        neighbor_values.append(distance_map[y + 1][x - 1])
                if y + 1 < len(distance_map) and x + 1 < len(distance_map[0]):
                    if distance_map[y + 1][x + 1] != -1:
                        neighbor_values.append(distance_map[y + 1][x + 1])

                if neighbor_values:
                    lowest_value_neighbor = neighbor_values[0]
                    for value in neighbor_values:
                        if lowest_value_neighbor > value:
                            lowest_value_neighbor = value

                    if distance_map[y][x] != -1:
                        if distance_map[y][x] > lowest_value_neighbor + 2:
                            distance_map[y][x] = lowest_value_neighbor + 1

        if distance_map == change:
            break
        else:
            change = []
            for y in range(0, len(distance_map)):
                row = []
                for x in range(0, len(distance_map[0])):
                    row.append(distance_map[y][x])
                change.append(row)
    return distance_map
    
def print_distance_map(distance_map):
    for y in range(0, len(distance_map)):
        for x in range(0, len(distance_map[0])):
            if(distance_map[y][x] == -1):
                sys.stdout.write('..')
            else:
                sys.stdout.write(str(distance_map[y][x]))
        sys.stdout.write('\n')
    
    for x in range(0, len(distance_map[0])):
        sys.stdout.write('--')
    sys.stdout.write('\n')
