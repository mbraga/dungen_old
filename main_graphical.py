#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
from archivio_stanze import Dizionario
from map_generator import Dungeon
from map_loader import Map
import pickle
import pygame
import random
import time

# Original comment and author:
#
###########################################
####### Dungeon Generator -- main.py ######
###########################################
####### Breiny Games (c) 2011 #############
###########################################
## This file uses functions from the     ##
## 'map_generator.py' module to generate ##
## the layout of a random dungeon, which ##
## is then read in by the 'map_loader.py'##
## module and displayed visually in a    ##
## pygame window. Documentation is in    ##
## 'map_generator.py' module.            ##
###########################################
###########################################
## Created by Christopher Breinholt      ##
## Breiny Games (c) 2011                 ##
## http://breinygames.blotspot.com/      ##
##                                       ##
## To Use: Run the main.py file.         ##
## Press spacebar to generate a new      ##
## dungeon. Press return to print the    ##
## stats to the shell. Press S to save   ##
## the dungeon. Press L to load.         ##
##                                       ##
## You can also run the text_only.py     ##
## file to just print generated dungeons ##
## the shell instead of rendered on the  ##
## screen in a pygame window. This kind  ##
## of gives you a better idea of how     ##
## the generator actually works and what ##
## it does.                              ##
###########################################

def main():
    print (" --------------------------------------------- ")
    print ("| Dungeon Generator for The Gate Mud          |")
    print (" --------------------------------------------- ")
    # Variabili di carattere generale.
    name = "dungeon"
    dungeon = []

    # Configurazione del Dungeon.
    dimensione_livelli = 2
    profondita_dungeon = 2
    difficolta_incontri = 3
    
    
    mud_rooms_x = 30
    mud_rooms_y = 30
    # Imposto la dimensione di visualizzazine di una tile.
    tile_w = 10
    tile_h = 10
    
    # Imposto la Dimensione dei Livelli.
    if(dimensione_livelli == 0):
        max_num_struc = 15
        min_num_rooms = 2
        min_room_size = (2, 2)
        max_room_size = (3, 3)
        min_corr_size = 2
        max_corr_size = 3
    elif(dimensione_livelli == 1):
        max_num_struc = 30
        min_num_rooms = 4
        min_room_size = (3, 3)
        max_room_size = (4, 4)
        min_corr_size = 2
        max_corr_size = 3
    elif(dimensione_livelli == 2):
        max_num_struc = 60
        min_num_rooms = 6
        min_room_size = (3, 3)
        max_room_size = (5, 5)
        min_corr_size = 3
        max_corr_size = 5
    
    # Imposto la Profondità del Dungeon.
    if(profondita_dungeon == 0):
        livelli = random.randint(1, 3)
    elif(profondita_dungeon == 1):
        livelli = random.randint(2, 5)
    elif(profondita_dungeon == 2):
        livelli = random.randint(3, 7)
    else:
        livelli = profondita_dungeon
        
    # Scelgo la tipologia di dungeon.
    tipi_dungeon = Dizionario.getDungeon_Types()
    scelta = random.randint(0, len(tipi_dungeon) - 1)
    tipo_dungeon = tipi_dungeon[scelta]
    
    # Imposto le variabili per la configurazione di PyGame.
    resolution_x = (mud_rooms_x * 2 + 1) * tile_w
    resolution_y = (mud_rooms_y * 2 + 1) * tile_h

    pygame.init()

    resolution = (resolution_x, resolution_y)
    screen = pygame.display.set_mode(resolution)
    pygame.display.set_caption("Random Dungeon Generator")
    clock = pygame.time.Clock()
    mappa = Map()
    
    # Genero un dungeon iniziale.
    print ("  - Generating dungeon...")
    start = time.time()
    for livello in range(1, livelli + 1):
        dungeon_level = Dungeon((mud_rooms_x, mud_rooms_y), name, livello, livelli, tipo_dungeon, max_num_struc, min_num_rooms, min_room_size, max_room_size, min_corr_size, max_corr_size, (tile_w, tile_h))
        dungeon_level.generate_dungeon()
        dungeon.append(dungeon_level)
    end = time.time()
    print ("  - Total ellapsed time :" + str(round(end - start, 2)) + " seconds")
    
    dungeon_current_level = 0
    dungeon_level = dungeon[dungeon_current_level]
    mappa.load_dungeon(dungeon_level)

    # Inizio un cilco loop per gestire gli input dell'utente.
    """
        Mappa dei tasti:
            INVIO  - Genera un nuovo dungeon.
            FR-SU  - Sali di un livello se possibile.
            FR-GIU - Scendi di un livello se possibile.
            g      - Genera i file per The Gate.
            s      - Salva il dungeon generato con dungen.
            l      - Carica un dungeon generato con dungen.
            r      - Rimuove nel livello visualizzato i corridoi inutili.
            i      - Visualizza le informazioni del livello nel terminale.
            p      - Visualizza il percorso tra le due scale.
    """
    running = True
    while running:
        clock.tick(10)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

                elif event.key == pygame.K_RETURN:
                    print (" --------------------------------------------- ")
                    print ("  - Generating dungeon...")
                    start = time.time()
                    dungeon = []
                    # Imposto la Profondità del Dungeon.
                    if(profondita_dungeon == 0):
                        livelli = random.randint(1, 3)
                    elif(profondita_dungeon == 1):
                        livelli = random.randint(2, 5)
                    elif(profondita_dungeon == 1):
                        livelli = random.randint(3, 7)
                    # Scelgo la tipologia di dungeon.
                    tipi_dungeon = Dizionario.getDungeon_Types()
                    scelta = random.randint(0, len(tipi_dungeon) - 1)
                    tipo_dungeon = tipi_dungeon[scelta]
                    # Genero un dungeon.
                    for livello in range(1, livelli + 1):
                        dungeon_level = Dungeon((mud_rooms_x, mud_rooms_y), name, livello, livelli, tipo_dungeon, max_num_struc, min_num_rooms, min_room_size, max_room_size, min_corr_size, max_corr_size, (tile_w, tile_h))
                        dungeon_level.generate_dungeon()
                        dungeon.append(dungeon_level)
                    
                    dungeon_current_level = 0
                    dungeon_level = dungeon[dungeon_current_level]
                    mappa.load_dungeon(dungeon_level)
                    end = time.time()
                    print ("  - Total ellapsed time :" + str(round(end - start, 2)) + " seconds")
                    
                elif event.key == pygame.K_UP:
                    if(dungeon_current_level > 0):
                        dungeon_current_level -= 1
                        dungeon_level = dungeon[dungeon_current_level]
                        mappa.load_dungeon(dungeon_level)
                    
                elif event.key == pygame.K_DOWN:
                    if(dungeon_current_level < len(dungeon) - 1):
                        dungeon_current_level += 1
                        dungeon_level = dungeon[dungeon_current_level]
                        mappa.load_dungeon(dungeon_level)

                elif event.key == pygame.K_g:
                    start = time.time()
                    # Genero il file stanze.
                    save = open("stanze." + name, "w")
                    for dungeon_temp in dungeon:
                        save.write(dungeon_temp.print_rooms_for_mud())
                    save.close()
                    
                    # Genero i file zona e mobs.
                    save = open("zona", "w")
                    save.write("Nome \"" + name + "\"\n")
                    save.write("Area \"" + name + "\"\n")
                    save.write("Evento iniz\n")
                    save.write("    Esegui proc($zona)\n")
                    save.close()
                    for livello in range(0, len(dungeon)):
                        save = open("mobs." + name + "_level_" + str(livello), "w")
                        save.write(dungeon[livello].print_mobs_for_mud(difficolta_incontri, livelli))
                        save.close()
                    save = open("zona", "a")
                    save.write("    fine\n")
                    save.write("fineevento\n")
                    save.close()
                    end = time.time()
                    
                    # Genero il file oggetti.
                    """
                    Creo il file oggetti che conterra' la cassa utilizzata per distribuire i compensi.
                    """
                    file_casse = open("oggetti.cassa", "w")
                    file_casse.write("Oggetto \"cassa_antica_"+name+"\"\n")
                    file_casse.write("Nome { \"cassa\", \"antica\",\"$m\" }\n")
                    file_casse.write("Descbr \"una cassa antica\"\n")
                    file_casse.write("Descst \"Una robusta cassa dall'aspetto molto antico.\"\n")
                    file_casse.write("Desc\n")
                    file_casse.write("\"E' una cassa in legno$M, rinforzata da alcune lamine e borchie in metallo oramai arrugginite.\"\n")
                    file_casse.write("Tipo arredo\n")
                    file_casse.write("Peso 40\n")
                    file_casse.write("Costo 40\n")
                    file_casse.write("Cond 200 200\n")
                    file_casse.write("Funz contenitore 140 5 0 0\n")
                    file_casse.write("Compos (faggio)\n")
                    file_casse.write("Icona 256\n")
                    file_casse.write("Fineogg\n")
                    file_casse.close()
                    print ("  - The Gate files generated in : " + str(round(end - start, 2)) + " seconds")

                elif event.key == pygame.K_s:
                    save = open("saved_dungeon.txt", "w")
                    pickle.dump(dungeon, save)
                    save.close()

                elif event.key == pygame.K_l:
                    save = open("saved_dungeon.txt", "r")
                    dungeon = pickle.load(save)
                    save.close()
                    dungeon_current_level = 0
                    dungeon_level = dungeon[dungeon_current_level]
                    mappa.load_dungeon(dungeon_level)
                    
                elif event.key == pygame.K_r:
                    dungeon_level.remove_useless_corridor()
                    mappa.load_dungeon(dungeon_level)
                    
                elif event.key == pygame.K_i:
                    dungeon_level.print_info()

        mappa.draw(screen)
        pygame.display.flip()

    pygame.quit()

if __name__ == "__main__":
    main()
