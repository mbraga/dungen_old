#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
from astar import Pathfinder
from distance_map import distance_map
from distance_map import print_distance_map
from mud import MudGenerator, MudRoom, MudExit
import random
import time

###########################################
## Dungeon Generator -- map_generator.py ##
###########################################
####### Breiny Games (c) 2011 #############
###########################################
## This file contains the classes and    ##
## functions used to generate a random   ##
## dungeon in plain text which can be    ##
## used by either the 'map_loader' module##
## to visually display the dungeon in a  ##
## pygame window or by the 'text_only.py'##
## module to print the map in plain text ##
###########################################

class Room:
    """
    Questa classe permette di definire una stanza, cioe' una matrice di caselle contigue.
    Argomenti:
        tipo
            - il tipo di stanza[ROOM, CORRIDOR].

        (size_x, size_y)
            - la dimensione della stanza.

        tiles
            - una lista 2D che contiene i numeri che raprresentano le tiles all'interno della stanza.

    Note:
        Le caselle possono essere:
            0  = Blank space
            1  = Floor
            2  = Corner
            3  = Wall facing NORTH
            4  = Wall facing EAST
            5  = Wall facing SOUTH
            6  = Wall facing WEST
            7  = Door
            8  = Stairs leading to a higher lever in the dungeon
            9  = Stairs leading to a lower level in the dungeon
            10 = Chest
            11 = Path from up to down staircases
            12 = Corridor tile
    """

    def __init__(self, tipo, size, dungeon, livello, tiles):
        self.tipo = tipo
        self.size = size
        self.dungeon = dungeon
        self.livello = livello
        self.tiles = tiles
        self.position = None
        self.mud_rooms = []
        self.rooms_status = None

    def generate_mud_rooms(self, grid, dungeon_type):
        """
        Visto e considerato che questa classe si riferisce ad una stanza, quindi il corrispettivo di piu' stanze in TheGate,
        questo metodo genera un nome e una descrizione da associare a tutte loro, inoltre aggiunge dove necessario le uscite.
        """
        nome = MudGenerator.generate_room_name(self, dungeon_type)
        descrizione = MudGenerator.generate_room_description(dungeon_type, nome[1], nome[2], nome[3])
        self.rooms_status = nome[3]
        for y in range(1, self.size[1], 2):
            for x in range(1, self.size[0], 2):
                srx = self.position[0] + x
                sry = self.position[1] + y

                assert(srx % 2 == 1)
                assert(sry % 2 == 1)

                if grid[int(sry)][int(srx)] == 8:
                    mr = MudRoom(self.dungeon + "_livello_" + str(self.livello) + "_stair_up", nome[0], descrizione, "interno", False)
                    mr.add_exit(MudExit("alto", self.dungeon + "_livello_" + str(self.livello - 1) + "_stair_down", "#", "#"))
                elif grid[int(sry)][int(srx)] == 9:
                    mr = MudRoom(self.dungeon + "_livello_" + str(self.livello) + "_stair_down", nome[0], descrizione, "interno", False)
                    mr.add_exit(MudExit("basso", self.dungeon + "_livello_" + str(self.livello + 1) + "_stair_up", "#", "#"))
                elif grid[int(sry)][int(srx)] == 10:
                    mr = MudRoom(self.dungeon + "_livello_" + str(self.livello) + "_stanza_" + str(srx) + "_" + str(sry), nome[0], descrizione, "interno", True)
                else:
                    mr = MudRoom(self.dungeon + "_livello_" + str(self.livello) + "_stanza_" + str(srx) + "_" + str(sry), nome[0], descrizione, "interno", False)

                dirs = [(0, -1), (1, 0), (0, 1), (-1, 0)]
                for d in range(0, len(dirs)):
                    dir_coord = dirs[d]
                    if grid[int(sry + dir_coord[1])][int(srx + dir_coord[0])] not in [0, 2, 3, 4, 5, 6]:
                        mr.add_exit(MudExit(d, self.dungeon + "_livello_" + str(self.livello) + "_stanza_" + str(srx + dir_coord[0] * 2) + "_" + str(sry + dir_coord[1] * 2), "#", "#"))

                self.mud_rooms.append(mr)
        return self.mud_rooms

    def get_mud_rooms(self):
        """
        Ritorna le stanze generate per TheGate.
        """
        return self.mud_rooms

class Dungeon:
    """
    Classe che contiene tutte le informazioni del dungeon come anche i metodi per modellarlo.
    Argomenti:
        room_size
            - Indica la grandezza indicativa delle stanze.
            
        name
            - Il nome del dungeon.
        
        livello
            - Il livello attuale.
            
        livelli
            - Il numero totale di livelli.
        
        dungeon_type
            - Il tipo di dungeon che si vuole generare.
            
        max_num_rooms
            - Il massimo numero di stanze.
            
        min_room_size, max_room_size
            - La dimensione minima e massima di una stanza.
            
        min_corr_size, max_corr_size
            - La dimensione minima e massima di una stanza.
            
        tile_size
            - La dimensione di una tile.
    """
    def __init__(self, room_size, name, livello, livelli, dungeon_type, max_num_rooms, min_num_rooms, min_room_size, max_room_size, min_corr_size, max_corr_size, tile_size):

        self.mud_rooms = (room_size[0], room_size[1])
        self.grid_size = (room_size[0] * 2 + 1, room_size[1] * 2 + 1)
        self.name = name
        self.livello = livello
        self.livelli = livelli
        self.max_num_rooms = max_num_rooms
        self.min_num_rooms = min_num_rooms
        self.min_room_size = min_room_size
        self.max_room_size = max_room_size
        self.min_corr_size = min_corr_size
        self.max_corr_size = max_corr_size
        self.tile_w = tile_size[0]
        self.tile_h = tile_size[1]
        self.rooms = []
        self.grid = []
        self.mud_rooms = []
        self.num_rooms = 0
        self.num_corridors = 0
        self.dungeon_type = dungeon_type
        self.stairs_up = None
        self.stairs_down = None
        
        # Variabili per gestire il path fra le scale.
        self.path = (room_size[0] * 2 + 1, room_size[1] * 2 + 1)

    def generate_mud_rooms(self):
        """
        Richiama il metodo per generare le stanze del dungeon.
        """
        self.mud_rooms = []
        for room in self.rooms:
            self.mud_rooms += room.generate_mud_rooms(self.grid, self.dungeon_type)

    def print_rooms_for_mud(self):
        """
        Ritorna la stringa che corrisponde a delle stanze per TheGate.
        """
        rooms = ""
        for mud_room in self.mud_rooms:
            rooms += mud_room.print_for_mud()
        return rooms

    def print_mobs_for_mud(self, difficolta_incontri, livelli):
        """
        Genera una serie di incontri per il dungeon, li posiziona e ritorna una stringa contenente i mob in un formato adatto a TheGate.
        """
        ## Variabili generali. ##
        mobs_for_file = ""
        
        ## Definisco i possibili incontri. ##
        # Principiante #
        if(difficolta_incontri == 0):
            possibili_gruppi = [["MN"],
                                ["MF", "MF"]]
            livello_boss = ["BF"]
        # Avventuriero #
        elif(difficolta_incontri == 1):
            possibili_gruppi = [["MA"],
                                ["MN", "MN"],
                                ["MN", "MF", "MF"],
                                ["MF", "MF", "MF", "MF"]]
            livello_boss = ["BF"]
        # Esperto #
        elif(difficolta_incontri == 2):
            possibili_gruppi = [["MA", "MN", "MN"],
                                ["MN", "MN", "MN", "MN"],
                                ["MA", "MF", "MF", "MF"]]
            livello_boss = ["BN"]
        # Sbruffone #
        elif(difficolta_incontri == 3):
            possibili_gruppi = [["BN", "MA"],
                                ["BF", "MA", "MA"],
                                ["MA", "MA", "MA"],
                                ["MA", "MA", "MN", "MN"]]
            livello_boss = ["BA"]

        # Piazzo i gruppi di mob nelle stanze, evitando la stanza con le scale in discesa, dove andrà messo il Boss.
        for i in range(0, len(self.rooms)):
            # Controllo si tratti di una stanza e non di un corridoio.
            if(self.rooms[i].tipo == "ROOM"):
                if(self.rooms[i] != self.stairs_down or (difficolta_incontri == 0 and self.livello < livelli)):
                    ## Posiziono un gruppo di Mob.##
                    room = self.rooms[i]
                    
                    # Seleziono una stanza in cui piazzare il gruppo di mob e la stampo nel file zona.
                    mud_room_id = (random.choice(room.get_mud_rooms())).id
                    
                    # Stampo nel file zone la prima parte per il posizionamento dei mob.
                    save = open("zona", "a")
                    save.write("        $stanza = stanza(<<s:" + mud_room_id + "@" + self.name + ">>)\n")
                    save.close()
                    
                    # Definisco l'id del gruppo come la stanza in cui li ho fatti poppare.
                    group_id = str(mud_room_id)
                    
                    # Seleziono la conformazione del gruppo.
                    conformazione_gruppo = random.choice(possibili_gruppi)
                    
                    # Genero i mob in base alla difficoltà degli incontri.
                    mobs = MudGenerator.generate_mob_group(self.dungeon_type, room.rooms_status, group_id, conformazione_gruppo)
                    for mob in mobs:
                        mobs_for_file += mob.print_for_mud(self.name)
                    
                    # Stampo un carattere newline per distanziare i vari posizionamenti di mob nelle stanze.
                    save = open("zona", "a")
                    save.write("\n")
                    save.close()
                else:
                    # Posiziono un Boss.
                    ## Posiziono un gruppo di Mob.##
                    room = self.rooms[i]
                    
                    # Seleziono una stanza in cui piazzare il gruppo di mob e la stampo nel file zona.
                    mud_room_id = (random.choice(room.get_mud_rooms())).id
                    
                    # Stampo nel file zone la prima parte per il posizionamento dei mob.
                    save = open("zona", "a")
                    save.write("        $stanza = stanza(<<s:" + mud_room_id + "@" + self.name + ">>)\n")
                    save.close()
                    
                    # Definisco l'id del gruppo come la stanza in cui li ho fatti poppare.
                    group_id = str(mud_room_id)
                    
                    # Genero il boss in baso al suo Grado di Sfida.
                    mobs = MudGenerator.generate_mob_group(self.dungeon_type, room.rooms_status, group_id, livello_boss)
                    for mob in mobs:
                        mobs_for_file += mob.print_for_mud(self.name)
                    
                    # Stampo un carattere newline per distanziare i vari posizionamenti di mob nelle stanze.
                    save = open("zona", "a")
                    save.write("\n")
                    save.close()
        return mobs_for_file

    def print_info(self, grid=False):
        """
        Stampa a console le informazioni sul dungeon.
        """
        print ("-=Dungeon Info=-")
        print ("  Nome             : " + str(self.name))
        print ("  Livello          : " + str(self.livello))
        print ("  Livelli          : " + str(self.livelli))
        print ("  Dimensione       : " + str(self.grid_size[0]) + "x" + str(self.grid_size[1]))
        print ("  Numero Strutture : " + str(len(self.rooms)))
        print ("  Numero Stanze    : " + str(self.num_rooms))
        print ("  Numero Corridoi  : " + str(self.num_corridors))

    def set_chests(self, stanza):
        """
        Posiziona nella stanza una cassa.
        """
        stanza_tile = -1
        while(stanza_tile == -1 or stanza_tile == 9):
            cassa_y = int(random.randrange(1, self.stairs_down.size[1], 2) + self.stairs_down.position[1])
            cassa_x = int(random.randrange(1, self.stairs_down.size[0], 2) + self.stairs_down.position[0])
            stanza_tile = self.grid[cassa_y][cassa_x]
        
        if self.grid[cassa_y][cassa_x] == 1:
            self.grid[cassa_y][cassa_x] = 10
        else:
            print ("Errore nel piazzare la cassa!\n")

    # Mark 2 tiles as stairs up, down
    def set_staircases(self):
        """
        Posiziona le scale in discesa e in salita, utilizzando la distance_map andremo a
        posizinare le scale ad una distanza raginevole fra loro.
        """
        for i in range(0, len(self.rooms)):
            self.stairs_up = random.choice(self.rooms)
            if(self.stairs_up.tipo == "ROOM"):
                x = random.randrange(1, self.stairs_up.size[0], 2) + self.stairs_up.position[0]
                y = random.randrange(1, self.stairs_up.size[1], 2) + self.stairs_up.position[1]
                
                assert(x % 2 == 1)
                assert(y % 2 == 1)
                
                if self.grid[int(y)][int(x)] == 1:
                    self.grid[int(y)][int(x)] = 8
                    self.stairs_up.up = (int(x), int(y))
                    break
            else:
                i -= 1
        """
        for i in range(0, len(self.rooms)):
            self.stairs_down = self.rooms[i]
            if(self.stairs_down != self.stairs_up):
                x = random.randrange(1, self.stairs_down.size[0], 2) + self.stairs_down.position[0]
                y = random.randrange(1, self.stairs_down.size[1], 2) + self.stairs_down.position[1]

                assert(x % 2 == 1)
                assert(y % 2 == 1)

                if self.grid[y][x] == 1:
                    self.grid[y][x] = 9
                    self.stairs_down.down = (x,y)
                    break
        """
        mappa = distance_map(self.grid, [8], [0, 2, 3, 4, 5, 6])
        distances = []

        for y in range(0, len(mappa)):
            for x in range(0, len(mappa[0])):
                if mappa[y][x] not in distances and mappa[y][x] != -1:
                    distances.append(mappa[y][x])

#        print_distance_map(mappa)
#        print(str(distances))
        
        if len(distances) < 2:
            print ("Error calculating distances!\n")
            return
        else:
            average = max(distances)
#        print(str(average))

        possible_places = []

        for i in range(0, len(self.rooms)):
            room = self.rooms[i]
            if(room.tipo == "ROOM"):
                for y in range(1, room.size[1], 2):
                    for x in range(1, room.size[0], 2):
                        assert(x % 2 == 1)
                        assert(y % 2 == 1)
                        if mappa[int(y + room.position[1])][int(x + room.position[0])] >= average:
                            possible_places.append(room)

        while(True):
            location = None
            self.stairs_down = random.choice(possible_places)
            x = int(random.randrange(1, self.stairs_down.size[0], 2) + self.stairs_down.position[0])
            y = int(random.randrange(1, self.stairs_down.size[1], 2) + self.stairs_down.position[1])
            assert(x % 2 == 1)
            assert(y % 2 == 1)
            location = (x, y)
            if self.grid[int(location[1])][int(location[0])] == 1:
                break
            
        if(self.livello < self.livelli):
            """
            Se mi trovo all'ultimo livello, non voglio venano piazzate le scale che scendono,
            ma in questo punto del codice, la stanza che avrebbe dovuto contenerle e' gia' stata
            definita, poiche' in quella stanza ci deve essere il boss di fine dungeon.
            Il nome della stanza richiamera' comunque le scale in discesa nonostante non le contenga.
            """
            if self.grid[location[1]][location[0]] == 1:
                self.stairs_down.down = location
                self.grid[location[1]][location[0]] = 9
            else:
                print ("Error placing stairs_down!\n")

    # Find the path between the up and down stairs, change floor to 11
    def find_path_between_staircases(self):
        """
        Mediante un algoritmo di pathfinding controlliamo vi sia un percorso fra le scale.
        """
        astar = Pathfinder()

        # Find the path between start and end points
        self.path = astar.find_path(self.grid, self.stairs_up.position, self.stairs_down.position, [0, 2, 3, 4, 5, 6])
        
        # Mark path tiles with 11
        if self.path == None:
            print ("No path possible.\n")

    # Set a tile as room floor
    def connect_rooms(self, branching_pos, direction):
        """
        Posiziona una porta come connessione.
        """
        self.grid[branching_pos[1]][branching_pos[0]] = 7

    # Check if the room can be placed
    def space_for_new_room(self, new_room_size, new_room_position):
        """
        Controlla se nella posizione specificata vi sia effettivamente spazio per la stanza.
        """
        for y in range(new_room_position[1], new_room_position[1] + new_room_size[1]):
            for x in range(new_room_position[0], new_room_position[0] + new_room_size[0]):
                if x < 0 or x > self.grid_size[0] - 1:
                    return False
                if y < 0 or y > self.grid_size[1] - 1:
                    return False
                
                if self.grid[y][x] != 0 and self.grid[y][x] not in range(2, 7):
                    return False

        return True

    # Return the direction, depending on wall tile
    def get_branching_direction(self, branching_position):
        """
        Ritorna la direzione corrispondente alla parte sulla quale si vuole connettere la nuova stanza.
        """
        direction = None
        if self.grid[branching_position[1]][branching_position[0]] == 3:
            direction = "NORTH"
        elif self.grid[branching_position[1]][branching_position[0]] == 4:
            direction = "EAST"
        elif self.grid[branching_position[1]][branching_position[0]] == 5:
            direction = "SOUTH"
        elif self.grid[branching_position[1]][branching_position[0]] == 6:
            direction = "WEST"
        else:
            return False

        return direction

    # Get a random tile to be used as a door
    def get_branching_position(self):
        """
        Ritorna la posizione cartesiana (x, y) dove si potra' andare a posizionare il collegamento con la nuova stanza.
        """
        # Get a random corridor
        found = False
        for i in range(0, len(self.rooms)):
            if(self.rooms[i].tipo == "CORRIDOR"):
                found = True
        
        branching_room = random.choice(self.rooms)
        if(found == True):
            for i in range(0, len(self.rooms)):
                room_elegible = random.choice(self.rooms)
                if(room_elegible.tipo == "CORRIDOR"):
                    branching_room = room_elegible

        branching_tile_position = None
        branching_type = branching_room.tipo

        # Cycle on each tile of the room
        for i in range(0, branching_room.size[0] * branching_room.size[1]):

            # Get a random x and y position
            if branching_type == "ROOM":
                p = random.randrange(1, branching_room.size[0] * branching_room.size[1], 2)

                x = p % branching_room.size[0]
                y = p / branching_room.size[0]
            else:
                p = random.randrange(0, 1)
                if branching_room.size[0] > branching_room.size[1]:
                    x = branching_room.size[0] * p
                    y = 1
                else:
                    x = 1
                    y = branching_room.size[1] * p

                    
            x += branching_room.position[0]
            y += branching_room.position[1]

            # Check if it is a wall (corners are excluded)
            if self.grid[int(y)][int(x)] > 2:
                branching_tile_position = (int(x), int(y))
                break

        if branching_tile_position != None:
            assert (branching_tile_position[0] % 2 == 0 and branching_tile_position[1] % 2 == 1) or (branching_tile_position[0] % 2 == 1 and branching_tile_position[1] % 2 == 0)

        return (branching_tile_position, branching_type)

    # Set the room position and copy to the grid
    def place_room(self, room, position):
        """
        Posiziona la stanza nella griglia.
        """
        assert position[0] % 2 == 0
        assert position[1] % 2 == 0

        # Set position
        room.position = (position[0], position[1])

        # Copy room tiles
        room_tile_x = 0
        room_tile_y = 0
        for y in range(int(position[1]), int(position[1] + room.size[1])):
            for x in range(int(position[0]), int(position[0] + room.size[0])):
                self.grid[y][x] = room.tiles[room_tile_y][room_tile_x]
                room_tile_x += 1
            room_tile_y += 1
            room_tile_x = 0

    # Generate the room tiles as an array of arrays. Each y contains x
    # 1 = room floor
    # 2 = corner
    # 3 = north wall
    # 4 = east wall
    # 5 = south wall
    # 6 = west wall
    def generate_room(self, min_size, max_size):
        """
        Genera una stanza.
        """
        size_x = 2 * random.randint(min_size[0], max_size[0]) + 1
        size_y = 2 * random.randint(min_size[1], max_size[1]) + 1

        assert size_x % 2 == 1
        assert size_y % 2 == 1

        tiles = []

        for y in range(0, size_y):
            row = []
            for x in range(0, size_x):
                if x == 0 and y == 0:
                    row.append(2)
                elif x == size_x - 1 and y == 0:
                    row.append(2)
                elif x == 0 and y == size_y - 1:
                    row.append(2)
                elif x == size_x - 1 and y == size_y - 1:
                    row.append(2)
                elif y == 0:
                    row.append(3)
                elif x == size_x - 1:
                    row.append(4)
                elif y == size_y - 1:
                    row.append(5)
                elif x == 0:
                    row.append(6)
                else:
                    row.append(1)
            tiles.append(row)

        return Room("ROOM", (size_x, size_y), self.name, self.livello, tiles)

    # Like generate room but for long straight corridors
    def generate_corridor(self, direction, min_corr_size, max_corr_size):
        """
        Genera un corridoio.
        """
        corr_len = 2 * random.randint(min_corr_size, max_corr_size) + 1

        if direction == "NORTH" or direction == "SOUTH":
            size_x = 3
            size_y = corr_len
        else:
            size_x = corr_len
            size_y = 3

        assert size_x % 2 == 1
        assert size_y % 2 == 1

        tiles = []

        for y in range(0, size_y):
            row = []
            for x in range(0, size_x):
                if x == 0 and y == 0:
                    row.append(2)
                elif x == size_x - 1 and y == 0:
                    row.append(2)
                elif x == 0 and y == size_y - 1:
                    row.append(2)
                elif x == size_x - 1 and y == size_y - 1:
                    row.append(2)
                elif y == 0:
                    row.append(3)
                elif x == size_x - 1:
                    row.append(4)
                elif y == size_y - 1:
                    row.append(5)
                elif x == 0:
                    row.append(6)
                else:
                    row.append(12)
            tiles.append(row)

        return Room("CORRIDOR", (size_x, size_y), self.name, self.livello, tiles)

    def generate_dungeon_map(self):
        """
        Genera l'intera mappa del livello.
        """
        self.rooms = []
        self.grid = []

        assert self.grid_size[0] % 2 == 1
        assert self.grid_size[1] % 2 == 1
        
        # Create an array of arrays. Each y contains x
        for y in range(0, self.grid_size[1]):
            row = []
            for x in range(0, self.grid_size[0]):
                row.append(0)
            self.grid.append(row)
        
        # Generate the first room to be placed
        self.rooms.append(self.generate_room(self.min_room_size, self.max_room_size))

        # Place the first room in the middle of the grid
        px = self.grid_size[0] / 2 - (self.rooms[-1].size[0] / 2)
        py = self.grid_size[1] / 2 - (self.rooms[-1].size[1] / 2)
        
        px += px % 2
        py += py % 2
        
        self.place_room(self.rooms[-1], (px, py))
        choice = 0
        # Repeat for twice the dungeon size
        for i in range(0, (self.grid_size[0] * self.grid_size[1])):
            # Exit if max room is set
            if(self.num_rooms >= self.min_num_rooms): break

            # Get branching position and direction
            (branching_pos, branching_type) = self.get_branching_position()
            direction = self.get_branching_direction(branching_pos)
            if direction:
                # Generate a new room
                new_room_pos = (0, 0)
                self.count_rooms_corridors()

                if branching_type == "ROOM":
                    if (choice == 0):
                        new_room = self.generate_corridor(direction, self.min_corr_size, self.max_corr_size)
                        choice = 1
                    else:
                        new_room = self.generate_room(self.min_room_size, self.max_room_size)
                        choice = 0
                else:
                    new_room = self.generate_room(self.min_room_size, self.max_room_size)

                # Try to place the room
                if direction == "NORTH":
                    new_room_pos = (branching_pos[0] - random.randrange(1, new_room.size[0], 2),
                                    branching_pos[1] - (new_room.size[1] - 1))
                elif direction == "EAST":
                    new_room_pos = (branching_pos[0],
                                    branching_pos[1] - random.randrange(1, new_room.size[1], 2))
                elif direction == "SOUTH":
                    new_room_pos = (branching_pos[0] - random.randrange(1, new_room.size[0], 2),
                                    branching_pos[1])
                elif direction == "WEST":
                    new_room_pos = (branching_pos[0] - (new_room.size[0] - 1),
                                    branching_pos[1] - random.randrange(1, new_room.size[1], 2))

                # If there is space, place the room, add to the list of rooms and connect
                if self.space_for_new_room(new_room.size, new_room_pos):
                    self.place_room(new_room, new_room_pos)
                    self.rooms.append(new_room)
                    self.connect_rooms(branching_pos, direction)
                else:
                    i -= 1
            i += 1

    def remove_useless_corridor(self):
        """
        Metodo per la rimozione dei corridoi noti come vicoli ciechi.
        """
        corridors = [room for room in self.rooms if room.tipo == "CORRIDOR"]
        for corridor in corridors:
            door = 0
            for y in range(corridor.position[1], corridor.position[1] + corridor.size[1]):
                for x in range(corridor.position[0], corridor.position[0] + corridor.size[0]):
                    if(self.grid[y][x] == 7):
                        door += 1
            if(door == 1):
                for y in range(corridor.position[1], corridor.position[1] + corridor.size[1]):
                    for x in range(corridor.position[0], corridor.position[0] + corridor.size[0]):
                        if(self.grid[y][x] == 7):
                            self.grid[y][x] = self.rooms[self.rooms.index(corridor)].tiles[y - corridor.position[1]][x - corridor.position[0]]
                        if(self.grid[y][x] == 12):
                            self.grid[y][x] = 0
                self.rooms.remove(corridor)
        
        for y in range(0, len(self.grid)):
            for x in range(0, len(self.grid[0])):
                found = False
                """
                0 = blank space
                2 = corner tile
                3 = wall tile facing NORTH.
                4 = wall tile facing EAST.
                5 = wall tile facing SOUTH.
                6 = wall tile facing WEST.
                """
                if(y > 0):
                    tile = self.grid[y - 1][x]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(x > 0):
                    tile = self.grid[y][x - 1]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(x > 0 and y > 0):
                    tile = self.grid[y - 1][x - 1]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(y > 0 and x < len(self.grid[0]) - 1):
                    tile = self.grid[y - 1][x + 1]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(x > 0 and y < len(self.grid) - 1):
                    tile = self.grid[y + 1][x - 1]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(x < len(self.grid[0]) - 1):
                    tile = self.grid[y][x + 1]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(y < len(self.grid) - 1):
                    tile = self.grid[y + 1][x]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(x < len(self.grid[0]) - 1 and y < len(self.grid) - 1):
                    tile = self.grid[y + 1][x + 1]
                    if tile not in [0, 2, 3, 4, 5, 6]:
                        found = True
                if(found == False):
                    self.grid[y][x] = 0
        self.count_rooms_corridors()
        self.generate_mud_rooms()

    # Conta il numero di stanze e di corridoi.
    def count_rooms_corridors(self):
        """
        Conta il numero di stanze e di corridoi.
        """
        # Ricalcolo il numero di stanze e di corridoi.
        self.num_rooms = 0
        self.num_corridors = 0
        for room in self.rooms:
            if(room.tipo == "ROOM"):
                self.num_rooms += 1
            else:
                self.num_corridors += 1

    def generate_dungeon(self):
        """
        Richiama secondo un ordine predefinito i metodi di questa classe, generando cosi' il livello del dungeon.
        """
#        start = time.time()
        while(True):
            self.generate_dungeon_map()
            self.count_rooms_corridors()
            self.remove_useless_corridor()
            self.set_staircases()
            if(self.livello < self.livelli):
                self.find_path_between_staircases()
            if(self.num_rooms >= self.min_num_rooms): break
        self.set_chests(self.stairs_down)
        self.generate_mud_rooms()
#        end = time.time()
#        print ("    - Livello "+str(self.livello)+" generato in : " + str(round(end-start, 2)) + " secondi.")
