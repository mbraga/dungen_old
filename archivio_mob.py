#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
class RazzaCustom:
    """
    Crea una razza secondo le proprie esigenze.
    """
    def __init__(self, nome, nome_articolo_m, nome_articolo_f, force_gender, razza, desc, tipologia, carat, zone_ambienti):
        self.nome = nome
        self.nome_articolo_m = nome_articolo_m
        self.nome_articolo_f = nome_articolo_f
        self.force_gender = force_gender
        self.razza = razza
        self.desc = desc
        self.tipologia = tipologia
        self.carat = carat
        self.zone_ambienti = zone_ambienti
        
    def getNome(self):
        """
        Ritorna il nome generico della razza.
        Esempio:
            Eliantiriano
        """
        return self.nome
        
    def getNomeArticolato(self, gender):
        """
        Ritorna la razza con l'aggiunta dell'articolo, per poter essere utilizzata in alcune frasi.
        Esempio:
            Un eliantiriano
        """
        if(self.force_gender == "masch"):
            return self.nome_articolo_m
        elif(self.force_gender == "femmina"):
            return self.nome_articolo_f

        if(gender == "masch"):
            return self.nome_articolo_m
        elif(gender == "femmina"):
            return self.nome_articolo_f
        
    def getRazza(self):
        """
        Ritorna il codice razza di TheGate associato a questa.
        Esempio:
            uma
        """
        return self.razza
        
    def getDesc(self):
        """
        Ritorna una descrizione generica.
        """
        return self.desc
        
    def getTipologia(self):
        """
        Ritorna qual'e' il tipo di specie di questa razza:
        Casi:
            0 - umanoide
            1 - animale
            2 - indefinito
        """
        return self.tipologia

    def getCarat(self):
        """
        Ritorna le caratteristiche di partenza di questa razza.
        """
        return self.carat
    
    def getZoneAmbienti(self):
        """
        Ritorna la lisa di zone e ambienti in cui si puo' trovare questa razza.
        Esempio:
            Un orco lo si puo' trovare in:
                 ["Underdark", "normale"]
            quindi nell'Underdark con un clima normale.
        """
        return self.zone_ambienti

class Razze:
    """
    Vengono forniti dei metodi per creare le razze e per selezionare quelle consone a determinate zone.
    """
    @staticmethod
    def generateRazze():
        """
        Genera le razze.
        """
        razze = []
        # Umanoidi
        """
        razza = RazzaCustom("Eliantiriano",
                            "Un eliantiriano",
                            "Un'eliantiriana",
                            None,
                            "uma",
                            "Ti trovi davanti un umano di statura media, pare proprio non gradire la tua presenza infatti ti sta fissando con sguardo accigliato.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Edifici", "normale"], ["Maniero", "normale"], ["Castello" , "normale"]])
        razze.append(razza)
        
        razza = RazzaCustom("Emeriano",
                            "Un emeriano",
                            "Un'emeriana",
                            None,
                            "ume",
                            "Ti trovi davanti un individuo dalla carnagione molto scura, la sua età è tradita dai suoi lineamenti oramai segnati dal tempo e da alcune ciocche di capelli ricci di colore grigio.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Edifici", "normale"], ["Maniero", "normale"], ["Castello" , "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Mezz'elfo",
                            "Un mezz'elfo",
                            "Una mezz'elfa",
                            None,
                            "mel",
                            "Ad un primo acchitto avresti giurato si trattasse di un elfo, osservando meglio però noti alcune somiglianze alla razza umana.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Edifici", "normale"], ["Maniero", "normale"], ["Castello" , "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Nano",
                            "Un nano",
                            "Una nano",
                            None,
                            "nan",
                            "Piccolo tarchiato e dal fare laborioso, non appena ti nota interrompe le sue attività e ti osserva con sguardo arcigno, ora si dedicherà a te con altrettanta dedizione.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Edifici", "normale"], ["Maniero", "normale"], ["Castello" , "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Halfling",
                            "Un halfling",
                            "Un'halfling",
                            None,
                            "hal",
                            "Ti trovi davanti un piccolo umanoide. I capelli, di un colore nero come la notte, sono raccolti in un codino tenuto insieme da un cordone intrecciato a mano.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Edifici", "normale"], ["Maniero", "normale"], ["Castello" , "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Elfo ambrato",
                            "Un elfo ambrato",
                            "Un'elfa ambrata",
                            None,
                            "dra",
                            "Quello che ti si para davanti è un elfo ambrato. Carnagione bronzea dovuta alla prolungata esposizione al sole, capelli lunghi raccolti in un'unica treccia tenuta assieme da una serie di anelli d'argento.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Edifici", "normale"], ["Maniero", "normale"], ["Castello" , "normale"]])
        razze.append(razza)
        """
        razza = RazzaCustom("Goblin",
                            "Un goblin",
                            "Una goblin",
                            None,
                            "gob",
                            "Ti è sembrato di vedere un goblin. E' vero! E' vero! E' proprio un goblin!",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Underdark", "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Elfo nero",
                            "Un elfo nero",
                            "Un'elfa nera",
                            None,
                            "drw",
                            "Un ombra si muove nell'oscurità. Due piccole fessure luminescenti si dischiudono, fissando ogni tuo movimento.",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Underdark", "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Orco",
                            "Un orco",
                            "Un'orchessa",
                            None,
                            "orc",
                            "Una montagna di muscoli, un cervello grande quanto una nocciolina, colora tutto di verde ed ecco, hai il tuo orco!",
                            0,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Underdark", "normale"]])
        razze.append(razza)

        # Animali.
        razza = RazzaCustom("Topo",
                            "Un topo",
                            None,
                            "masch",
                            "top",
                            "Piccolo, peloso e sporco.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Caverna", "normale"], ["Underdark", "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Lupo",
                            "Un lupo",
                            None,
                            "masch",
                            "lup",
                            "Si tratta di un comune lupo dal pelo scuro come la notte, non appena fiuta il tuo odore si mette in posizione, pronto ad attaccarti.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Caverna", "normale"], ["Underdark", "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Ragno",
                            "Un ragno",
                            None,
                            "masch",
                            "ara",
                            "Otto zampe, otto occhi, due zanne.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Underdark", "normale"]])
        razze.append(razza)


        razza = RazzaCustom("Orso",
                            "Un orso",
                            None,
                            "masch",
                            "lup",
                            "Ti trovi davanti un orso dal portamento fiero, il pelo castano e artigli lunghi ed affilati.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Caverna", "normale"], ["Underdark", "normale"]])
        razze.append(razza)


        razza = RazzaCustom("Salamandra",
                            None,
                            "Una salamandra",
                            "femmina",
                            "sal",
                            "Con un guizzo estrae la lingua e fiuta la zona, rilevando la tua presenza prima ancora che tu te ne accorga.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Caverna", "lavico"], ["Underdark", "lavico"]])
        razze.append(razza)

        razza = RazzaCustom("Pianta Carnivora",
                            None,
                            "Una pianta carnivora",
                            "femmina",
                            "pia",
                            "Uh che carina, una pianta... carnivora.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Caverna", "vegetale"], ["Underdark", "vegetale"]])
        razze.append(razza)

        razza = RazzaCustom("Ragno Gigante",
                            "Un ragno gigante",
                            None,
                            "masch",
                            "arg",
                            "Sedici zampe, sedici occhi, quattro zanne. E' un ragno formato delux.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Underdark", "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Pantera",
                            None,
                            "Una pantera",
                            "femmina",
                            "pnt",
                            "Guardando questo magnifico esemplare, non sai se scappare o rimanere immobile ad ammirare il suo mortale splendore.",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Underdark", "normale"]])
        razze.append(razza)

        razza = RazzaCustom("Verme Gigante",
                            "Un verme gigante",
                            None,
                            "masch",
                            "veg",
                            "Quello che da prima credevi fosse un costone di roccia, si rivela essere un verme, grande. No non scherzo, davvero grande!",
                            1,
                            [-100, -100, -100, -100, -100, -100, -100, -100, -100],
                            [["Caverna", "ghiacciato"], ["Underdark", "ghiacciato"]])
        razze.append(razza)
        return razze
    @staticmethod
    def getRazze_Zona_Status(razze, dungeon_zone, dungeon_status):
        """
        Argomenti:
            razze - le razze esistenti nel generatore.
            dungeon_zone - la zona di cui si vogliono conoscere le razze possibili.
            dungeon_status - lo status della zona in questione.
        Ritorna:
            la lista di razze accettate.
        """
        razze_accettate = []
        for razza in razze:
            zone_ambienti = razza.getZoneAmbienti()
            for zona_ambiente in zone_ambienti:
                if(zona_ambiente[0] == dungeon_zone and zona_ambiente[1] == dungeon_status):
                    razze_accettate.append(razza)
        return razze_accettate

class Modificatori:
    """
    Fornisce un metodo per ricevere il modificatore caratteristica in base al grado di sfida del mostro.
    """
    @staticmethod
    def applicaGradoSfida(gradosfida):
        """
        Ritorna la lista delle modifiche alle caratteristiche, secondo il format di TheGate:
            [for, cos, tag, des, vel, emp, int, pot, vol]
        """
        valore = 0
        if(gradosfida == "MF"):
            valore = +25
        elif(gradosfida == "MN"):
            valore = 0
        elif(gradosfida == "MA"):
            valore = -15
        elif(gradosfida == "BF"):
            valore = -30
        elif(gradosfida == "BN"):
            valore = -45
        elif(gradosfida == "BA"):
            valore = -60
        return [valore, valore, valore, valore, valore, valore, valore, valore, valore]
    
class Comportamenti:
    """
    Questa classe contiene tutti gli eventi attribuibili a mob di vario genere, dal gregario, al capo-gruppo oppure al boss.
    """
    @staticmethod
    def aggressivo_seguace(razze_alleate):
        """
        Ritorna il comportamento di un mob seguace, da abbinare ad un capo-gruppo.
        
        """
        comportamento = "Evento saluto 100\n"
        comportamento += "    proc aggressivo_seguace($mob, $chi)\n"
        comportamento += "        $combat     = combat($mob)\n"
        comportamento += "        $dio        = dio($chi)\n"
        comportamento += "        $pg         = pc($chi)\n"
        comportamento += "        $razza      = razza($chi)\n"
        comportamento += "        $visibile   = visib ($mob, $chi)\n"
        comportamento += "        $spirito    = spirito($chi)\n"
        comportamento += "        $stanza     = stanza ($mob)\n"
        comportamento += "        // Se il mob non è in combattimento eseguo, altrimenti niente.\n"
        comportamento += "        se $combat == 0\n"
        comportamento += "            // Se NO-DIO e NO-VISIBILE\n"
        comportamento += "            se $dio == 0 && $visibile == 0\n"
        comportamento += "                // Rispondono al comando di allerta del capobranco.\n"
        comportamento += "                aspetta(3)\n"
        comportamento += "                $random = caso(9)\n"
        comportamento += "                se $random < 3\n"
        comportamento += "                    esp sembra allarmato.\n"
        comportamento += "                altse $random < 5\n"
        comportamento += "                    guarda capobranco\n"
        comportamento += "                finese\n"
        comportamento += "            // se PG/PNG VISIBILE, NO-SPIRITO, NO-DIO\n"
        comportamento += "            altse $spirito == 0 && $dio == 0 && $visibile == 1\n"
        comportamento += "                se $pg == 1 && ("
        for i in range(0, len(razze_alleate)):
            if(i < len(razze_alleate) - 1):
                comportamento += "$razza == num_razza(\"" + razze_alleate[i] + "\") || "
            else:
                comportamento += "$razza == num_razza(\"" + razze_alleate[i] + "\"))\n"
        comportamento += "                    // Osservano il capobranco in attesa di ordini.\n"
        comportamento += "                    $random = caso(9)\n"
        comportamento += "                    se $random < 5\n"
        comportamento += "                        guarda capobranco\n"
        comportamento += "                    finese\n"
        comportamento += "                finese\n"
        comportamento += "            finese\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "    Esegui proc($mob, $chi)\n"
        comportamento += "        aggressivo_seguace($mob, $chi)\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n\n"
        return comportamento

    @staticmethod
    def aggressivo_capo(razze_alleate):
        """
        Ritorna il comportamento di un mob capo-gruppo, solitamente da abbinare a dei seguaci.
        Si possono mettere più capi gruppo assieme.
        """
        comportamento = "Evento saluto 100\n"
        comportamento += "    proc aggressivo_capo($mob, $chi)\n"
        comportamento += "        $combat     = combat($mob)\n"
        comportamento += "        $dio        = dio($chi)\n"
        comportamento += "        $pg         = pc($chi)\n"
        comportamento += "        $razza      = razza($chi)\n"
        comportamento += "        $visibile   = visib ($mob, $chi)\n"
        comportamento += "        $spirito    = spirito($chi)\n"
        comportamento += "        $stanza     = stanza ($mob)\n"
        comportamento += "        // Se il mob non è in combattimento eseguo, altrimenti niente.\n"
        comportamento += "        se $combat == 0\n"
        comportamento += "            // se NO-DIO e NO-VISIBILE\n"
        comportamento += "            se $dio == 0 && $visibile == 0\n"
        comportamento += "                espr sembra essersi allertato per un qualche rumore.\n"
        comportamento += "            // se PG/PNG VISIBILE, NO-SPIRITO, NO-DIO\n"
        comportamento += "            altse $spirito == 0 && $dio == 0 && $visibile == 1\n"
        comportamento += "                se $pg == 1 || "
        for i in range(0, len(razze_alleate)):
            if(i < len(razze_alleate) - 1):
                comportamento += "$razza == num_razza(\"" + razze_alleate[i] + "\") || "
            else:
                comportamento += "$razza == num_razza(\"" + razze_alleate[i] + "\")\n"
        comportamento += "                    uccidi $chi\n"
        comportamento += "                    excmd($mob@sca1,cmdnum(\"assisti\"), \"capobranco\")\n"
        comportamento += "                    excmd($mob@sca2,cmdnum(\"assisti\"), \"capobranco\")\n"
        comportamento += "                    excmd($mob@sca3,cmdnum(\"assisti\"), \"capobranco\")\n"
        comportamento += "                    excmd($mob@sca4,cmdnum(\"assisti\"), \"capobranco\")\n"
        comportamento += "                finese\n"
        comportamento += "            finese\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "    Esegui proc($mob, $chi)\n"
        comportamento += "        aggressivo_capo($mob, $chi)\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n"
        comportamento += "\n"
        comportamento += "Evento ingresso 100"
        comportamento += "    Esegui proc($mob)\n"
        comportamento += "        $stanza = stanza ($mob)\n"
        comportamento += "        // Recupero l'ultimo pg/png entrato nella stanza, in genere il mob stesso.\n"
        comportamento += "        $chi    = p_pers_in_st($stanza)\n"
        comportamento += "        // Ciclo su tutti i pg/png in cerca di uno bono da attaccare.\n"
        comportamento += "        ripetise $chi != $oldChi\n"
        comportamento += "            $dio        = dio($chi)\n"
        comportamento += "            $spirito    = spirito($chi)\n"
        comportamento += "            se $chi == $mob || $chi == $mob@sca1 || $chi == $mob@sca2 || $chi == $mob@sca3 || $chi == $mob@sca4 || $dio == 1 || $spirito == 1\n"
        comportamento += "                // Recupero il prossimo pg/png nella stanza.\n"
        comportamento += "                $oldChi = $chi\n"
        comportamento += "                $chi    = succ($chi)\n"
        comportamento += "                se $chi == 0\n"
        comportamento += "                    //Controllo di non aver finito i MOB nella stanza. Forzo il ciclo alla FINE!\n"
        comportamento += "                    $oldChi = $chi \n"
        comportamento += "                finese\n"
        comportamento += "            altrimenti\n"
        comportamento += "                aggressivo_capo($mob, $chi)\n"
        comportamento += "                //Forzo la fine del ciclo!\n"
        comportamento += "                $oldChi = $chi\n"
        comportamento += "            finese\n"
        comportamento += "        finerip\n"
        comportamento += "    fine\n"
        comportamento += "Fineevento\n\n"
        return comportamento
    
    @staticmethod
    def aggressivo_boss(razze_alleate):
        """
        Ritorna il comportamento di un mob boss.
        Un boss lo si puo' mette in coppia con altri mob.
        """
        comportamento = "Evento saluto 100\n"
        comportamento += "    proc aggressivo_boss($mob, $chi)\n"
        comportamento += "        $combat     = combat($mob)\n"
        comportamento += "        $dio        = dio($chi)\n"
        comportamento += "        $pg         = pc($chi)\n"
        comportamento += "        $razza      = razza($chi)\n"
        comportamento += "        $visibile   = visib ($mob, $chi)\n"
        comportamento += "        $spirito    = spirito($chi)\n"
        comportamento += "        $stanza     = stanza ($mob)\n"
        comportamento += "        // Se il mob non è in combattimento eseguo, altrimenti niente.\n"
        comportamento += "        se $combat == 0\n"
        comportamento += "            // se NO-DIO e NO-VISIBILE\n"
        comportamento += "            se $dio == 0 && $visibile == 0\n"
        comportamento += "                espr sembra essersi allertato per un qualche rumore.\n"
        comportamento += "            // se PG/PNG VISIBILE, NO-SPIRITO, NO-DIO\n"
        comportamento += "            altse $spirito == 0 && $dio == 0 && $visibile == 1\n"
        comportamento += "                se $pg == 1 || "
        for i in range(0, len(razze_alleate)):
            if(i < len(razze_alleate) - 1):
                comportamento += "$razza == num_razza(\"" + razze_alleate[i] + "\") || "
            else:
                comportamento += "$razza == num_razza(\"" + razze_alleate[i] + "\")\n"
        comportamento += "                    uccidi $chi\n"
        comportamento += "                finese\n"
        comportamento += "            finese\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "    Esegui proc($mob, $chi)\n"
        comportamento += "        aggressivo_boss($mob, $chi)\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n"
        comportamento += "\n"
        comportamento += "Evento ingresso 100"
        comportamento += "    Esegui proc($mob)\n"
        comportamento += "        $stanza = stanza ($mob)\n"
        comportamento += "        // Recupero l'ultimo pg/png entrato nella stanza, in genere il mob stesso.\n"
        comportamento += "        $chi    = p_pers_in_st($stanza)\n"
        comportamento += "        // Ciclo su tutti i pg/png in cerca di uno bono da attaccare.\n"
        comportamento += "        ripetise $chi != $oldChi\n"
        comportamento += "            $dio        = dio($chi)\n"
        comportamento += "            $spirito    = spirito($chi)\n"
        comportamento += "            se $chi == $mob || $dio == 1 || $spirito == 1\n"
        comportamento += "                // Recupero il prossimo pg/png nella stanza.\n"
        comportamento += "                $oldChi = $chi\n"
        comportamento += "                $chi    = succ($chi)\n"
        comportamento += "                se $chi == 0\n"
        comportamento += "                    //Controllo di non aver finito i MOB nella stanza. Forzo il ciclo alla FINE!\n"
        comportamento += "                    $oldChi = $chi \n"
        comportamento += "                finese\n"
        comportamento += "            altrimenti\n"
        comportamento += "                aggressivo_boss($mob, $chi)\n"
        comportamento += "                //Forzo la fine del ciclo!\n"
        comportamento += "                $oldChi = $chi\n"
        comportamento += "            finese\n"
        comportamento += "        finerip\n"
        comportamento += "    fine\n"
        comportamento += "Fineevento\n\n"
        return comportamento

    @staticmethod
    def combattimento_seguace(razze_alleate):
        """
        Ritorna il comportamento in combattimento di un seguace.
        """
        comportamento = "evento combatti 25\n"
        comportamento += "    esegui proc($mob, $pers)\n"
        comportamento += "        $num = caso(1)\n"
        comportamento += "        se     $num == 0\n"
        comportamento += "            comb normale\n"
        comportamento += "        altse  $num == 1\n"
        comportamento += "            comb difensivo\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n\n"
        return comportamento

    @staticmethod
    def combattimento_capo(razze_alleate):
        """
        Ritorna il comportamento in combattimento di un capo-gruppo.
        """
        comportamento = "evento combatti 25\n"
        comportamento += "    esegui proc($mob, $pers)\n"
        comportamento += "        $num = caso(2)\n"
        comportamento += "        se     $num == 0\n"
        comportamento += "            ringhia\n"
        comportamento += "            comb berserk\n"
        comportamento += "        altse  $num == 1\n"
        comportamento += "            comb difensivo\n"
        comportamento += "        altse  $num == 2\n"
        comportamento += "            comb normale\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n\n"
        return comportamento
      
    @staticmethod
    def combattimento_boss(razze_alleate):
        """
        Ritorna il comportamento in combattimento di un boss.
        """
        comportamento = "evento combatti 25\n"
        comportamento += "    esegui proc($mob, $pers)\n"
        comportamento += "        $num = caso(2)\n"
        comportamento += "        se     $num == 0\n"
        comportamento += "            comb berserk\n"
        comportamento += "            esp si fa più minaccioso.\n"
        comportamento += "        altse  $num == 1\n"
        comportamento += "            comb difensivo\n"
        comportamento += "            esp rallenta il ritmo dei suoi colpi.\n"
        comportamento += "        altse  $num == 2\n"
        comportamento += "            comb normale\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n\n"
        return comportamento

    @staticmethod
    def casuale():
        """
        Ritorna il comportamento casuale per un mob.
        """
        comportamento = "evento casuale 5\n"
        comportamento += "    esegui proc($mob, $pers)\n"
        comportamento += "        $num=caso(1)\n"
        comportamento += "        se    $num == 0\n"
        comportamento += "            esp si guarda attorno con aria minacciosa.\n"
        comportamento += "        altse $num == 1\n"
        comportamento += "            esp avanza verso di te.\n"
        comportamento += "        finese\n"
        comportamento += "    fine\n"
        comportamento += "fineevento\n\n"
        comportamento += "Finemob\n\n"
        return comportamento
        
class Equipaggiamento:
    """
    Tiene traccia degli equipaggiamenti per i vari mob.
    """
    @staticmethod
    def eq_seguace(razza):
        """
        Ritorna l'evento iniz per equipaggiare un mob seguace.
        """
        if(razza == "orc"):
            equip = "evento iniz\n"
            equip += "    esegui proc($mob)\n"
            equip += "        // |TESTA|\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_osso@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |COLLO|\n"
            equip += "        $ogg = carica_ogg(<<o:collare_spinato@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |CORPO|\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_squame@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |GAMBE|\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_orcheschi@locanda>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |PIEDI|\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_rinf@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |MANO_PRIMARIA|\n"
            equip += "        $num = caso(6)\n"
            equip += "        se $num == 0\n"
            equip += "            $ogg = carica_ogg(<<o:ascia_orchesca_pietra@lerovine>>)\n"
            equip += "        altse $num == 1\n"
            equip += "            $ogg = carica_ogg(<<o:mannaia_pesante@lerovine>>)\n"
            equip += "        altse $num == 2\n"
            equip += "            $ogg = carica_ogg(<<o:clava_orchi@lerovine>>)\n"
            equip += "        altse $num == 3\n"
            equip += "            $ogg = carica_ogg(<<o:bastone_puntuto@lerovine>>)\n"
            equip += "        altse $num == 4\n"
            equip += "            $ogg = carica_ogg(<<o:bastone_snodato@lerovine>>)\n"
            equip += "        altse $num == 5\n"
            equip += "            $ogg = carica_ogg(<<o:catena_chiodata_pesante@base>>)\n"
            equip += "        altse $num == 6\n"
            equip += "            $ogg = carica_ogg(<<o:lancia_orchi@lerovine>>)\n"
            equip += "        finese\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        // |MANO_SECONDARIA|\n"
            equip += "        $ogg = carica_ogg(<<o:scudo_orchesco_01@lerovine>>)\n"
            equip += "        muovi_in_mano($ogg,$mob)\n"
            equip += "    fine\n"
            equip += "fineevento\n\n"
        elif(razza == "gob"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:piuma_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_ossa@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:mantella_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:cappuccio_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:perizoma_goblin_01@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stiletto_osso@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
            
        elif(razza == "uma"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:spada_corta@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_gorgiera_pesante@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "ume"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:spada_corta@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_gorgiera_pesante@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "mel"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg=carica_ogg(<<o:spadalunga_elfi_01@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg=carica_ogg(<<o:cintura_elfica@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg=carica_ogg(<<o:stivali_elfici_01@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corpettocuoiobronzeo_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_bronzo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_bronzo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccioglorilbrant_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccioglorilbrant_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:mantellodorato_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "nan"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $num = caso(2)\n"
            equip += "        se $num==0\n"
            equip += "            $ogg = carica_ogg(<<o:ascia_b@personaggi>>)\n"
            equip += "        altse $num==1\n"
            equip += "            $ogg = carica_ogg(<<o:picozza_squarciacorazze@base>>)\n"
            equip += "        altrimenti\n"
            equip += "            $ogg = carica_ogg(<<o:martello_g@personaggi>>)\n"
            equip += "        finese\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:gorgiera_blindata@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:elmo_spaccaossa@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_druin_1@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:cotta_maglia_nanica_01@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccio_corno@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccio_corno@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_naniche@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:guanti_nani_01@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:cinturone_nanico@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "hal"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg (<<o:mantello_elegante@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:calzoni_lana@rocca_spiriti>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:cappello_piuma@rocca_spiriti>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spada_sendariana@villaggio_halfling>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:bracciali_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corpetto_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "dra"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corazza_metallo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:tunica@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:stivali_borchiati@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:scimitarra_drow@base>>)\n"
            equip += "        muovi_in_inv($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "drw"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:alabarda_03@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        return equip

    @staticmethod
    def eq_capo(razza):
        """
        Ritorna l'evento iniz per equipaggiare un mob capo-gruppo.
        """
        if(razza == "orc"):
            equip = "evento iniz\n"
            equip += "    esegui proc($mob)\n"
            equip += "        // |TESTA|\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_osso@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |COLLO|\n"
            equip += "        $ogg = carica_ogg(<<o:catena_orchesca@lerovine>>\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |CORPO|\n"
            equip += "        $ogg = carica_ogg(<<o:catafratta_ossa@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |GAMBE|\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_orcheschi@locanda>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |PIEDI|\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_rinf@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |MANO_PRIMARIA|\n"
            equip += "        $num = caso(6)\n"
            equip += "        se $num == 0\n"
            equip += "            $ogg = carica_ogg(<<o:ascia_orchesca_pietra@lerovine>>)\n"
            equip += "        altse $num == 1\n"
            equip += "            $ogg = carica_ogg(<<o:mannaia_pesante@lerovine>>)\n"
            equip += "        altse $num == 2\n"
            equip += "            $ogg = carica_ogg(<<o:clava_orchi@lerovine>>)\n"
            equip += "        altse $num == 3\n"
            equip += "            $ogg = carica_ogg(<<o:bastone_puntuto@lerovine>>)\n"
            equip += "        altse $num == 4\n"
            equip += "            $ogg = carica_ogg(<<o:bastone_snodato@lerovine>>)\n"
            equip += "        altse $num == 5\n"
            equip += "            $ogg = carica_ogg(<<o:catena_chiodata_pesante@base>>)\n"
            equip += "        altse $num == 6\n"
            equip += "            $ogg = carica_ogg(<<o:lancia_orchi@lerovine>>)\n"
            equip += "        finese\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        // |MANO_SECONDARIA|\n"
            equip += "        $ogg = carica_ogg(<<o:scudo_osso@lerovine>>)\n"
            equip += "        muovi_in_mano($ogg,$mob)\n"
            equip += "    fine\n"
            equip += "fineevento\n\n"
        elif(razza == "gob"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_rinf@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:mantella_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:cappuccio_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:perizoma_goblin_01@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stiletto_osso@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "uma"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:spadone@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_gorgiera_pesante@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "ume"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:spadone@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_gorgiera_pesante@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "mel"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg=carica_ogg(<<o:spadalunga_elfi_01@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg=carica_ogg(<<o:cintura_elfica@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg=carica_ogg(<<o:stivali_elfici_01@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corpettocuoiobronzeo_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_bronzo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_bronzo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccioglorilbrant_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccioglorilbrant_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:mantellodorato_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg=carica_ogg(<<o:scudo_triangolare_elfico@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "nan"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:mazza_ferrata_nani_01@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:scudo_torre_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg (<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:gorgiera_blindata_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccio_corno_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccio_corno_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:blindata_nanica_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_naniche_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_druin_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:elmo_spaccaossa_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "hal"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg (<<o:mantello_elegante@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:calzoni_lana@rocca_spiriti>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:cappello_piuma@rocca_spiriti>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spada_sendariana@villaggio_halfling>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:bracciali_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corpetto_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "dra"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corazza_metallo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:tunica@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:stivali_borchiati@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:scimitarra_drow@base>>)\n"
            equip += "        muovi_in_inv($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "drw"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:alabarda_02@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        return equip

    @staticmethod
    def eq_boss(razza):
        """
        Ritorna l'evento iniz per equipaggiare un mob boss.
        """
        if(razza == "orc"):
            equip = "evento iniz\n"
            equip += "    esegui proc($mob)\n"
            equip += "        // |TESTA|\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_osso@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |COLLO|\n"
            equip += "        $ogg = carica_ogg(<<o:catena_orchesca@lerovine>>\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |CORPO|\n"
            equip += "        $ogg = carica_ogg(<<o:catafratta_ossa@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |GAMBE|\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_orcheschi@locanda>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |PIEDI|\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_rinf@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        // |MANO_PRIMARIA|\n"
            equip += "        $num = caso(6)\n"
            equip += "        se $num == 0\n"
            equip += "            $ogg = carica_ogg(<<o:ascia_orchesca_pietra@lerovine>>)\n"
            equip += "        altse $num == 1\n"
            equip += "            $ogg = carica_ogg(<<o:mannaia_pesante@lerovine>>)\n"
            equip += "        altse $num == 2\n"
            equip += "            $ogg = carica_ogg(<<o:clava_orchi@lerovine>>)\n"
            equip += "        altse $num == 3\n"
            equip += "            $ogg = carica_ogg(<<o:bastone_puntuto@lerovine>>)\n"
            equip += "        altse $num == 4\n"
            equip += "            $ogg = carica_ogg(<<o:bastone_snodato@lerovine>>)\n"
            equip += "        altse $num == 5\n"
            equip += "            $ogg = carica_ogg(<<o:catena_chiodata_pesante@base>>)\n"
            equip += "        altse $num == 6\n"
            equip += "            $ogg = carica_ogg(<<o:lancia_orchi@lerovine>>)\n"
            equip += "        finese\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        // |MANO_SECONDARIA|\n"
            equip += "        $ogg = carica_ogg(<<o:scudo_osso@lerovine>>)\n"
            equip += "        muovi_in_mano($ogg,$mob)\n"
            equip += "    fine\n"
            equip += "fineevento\n\n"
        elif(razza == "gob"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_rinf@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:mantella_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:cappuccio_goblin@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:perizoma_goblin_01@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stiletto_osso@lerovine>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "uma"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:spadone@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_gorgiera_pesante@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "ume"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:spadone@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $guardia)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_spalline_pesanti@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:armatura_gorgiera_pesante@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "mel"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg=carica_ogg(<<o:spadalunga_elfi_01@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg=carica_ogg(<<o:cintura_elfica@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg=carica_ogg(<<o:stivali_elfici_01@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corpettocuoiobronzeo_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_bronzo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_bronzo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccioglorilbrant_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccioglorilbrant_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:mantellodorato_elda@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg=carica_ogg(<<o:scudo_triangolare_elfico@citta_elfi>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "nan"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:mazza_ferrata_nani_01@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:scudo_torre_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg (<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:gorgiera_blindata_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccio_corno_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spallaccio_corno_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:blindata_nanica_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_naniche_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_druin_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:elmo_spaccaossa_rune@villaggio_nani>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "hal"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg (<<o:mantello_elegante@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:calzoni_lana@rocca_spiriti>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:cappello_piuma@rocca_spiriti>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:spada_sendariana@villaggio_halfling>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:bracciali_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corpetto_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:schinieri_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "dra"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg (<<o:corazza_metallo@cittadella>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:elmo_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:maniche_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:tunica@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:stivali_borchiati@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "        $ogg = carica_ogg (<<o:scimitarra_drow@base>>)\n"
            equip += "        muovi_in_inv($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        elif(razza == "drw"):
            equip = "Evento iniz\n"
            equip += "    Esegui proc($mob)\n"
            equip += "        $ogg = carica_ogg(<<o:guanti_pelle@base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:corpetto_maglia@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:schinieri_metallo@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:stivali_guerra@aral_base>>)\n"
            equip += "        muovi_in_eq($ogg, $mob)\n"
            equip += "        $ogg = carica_ogg(<<o:alabarda_02@base>>)\n"
            equip += "        muovi_in_eq($ogg,$mob)\n"
            equip += "    Fine\n"
            equip += "Fineevento\n\n"
        return equip

class NomiSoprannomiBoss:
    """
    Questa classe tiene traccia dei vari nomi e soprannomi da assegnare ad un boss.
    """
    @staticmethod
    def nomi_boss(tipologia, sesso):
        """
        Ritorna i possibili nomi per un boss.
        """
        if(tipologia == 0):
            if(sesso == "masch"):
                nome = ["Akodrondotut", "Auiaeldu", "Authauronai", "Avango", "Caurimegbau", "Durgor", "Etur", "Fazglaug", "Hartulorglandr", "Korurtur", "Koth", "Langorth", "Nalinan", "Nalirmmeglu", "Ororoba", "Saetulalaug", "Than", "Vaugl", "Wergurt", "Weturorg"]
            else:
                nome = ["Caulrcha", "Drca", "Iana", "Nalinan", "Ormotha", "Weritua", "Kala", "Onana", "Rchia", "Rgorgurma", "Tuaba", "Sharorindrthi"]
        else:
            nome = ["Akodrondotut", "Auiaeldu", "Authauronai", "Avango", "Caurimegbau", "Durgor", "Etur", "Fazglaug", "Hartulorglandr", "Korurtur", "Koth", "Langorth", "Nalinan", "Nalirmmeglu", "Ororoba", "Saetulalaug", "Than", "Vaugl", "Wergurt", "Weturorg"]
        return nome

    @staticmethod
    def soprannomi_boss(tipologia, sesso):
        """
        Ritorna i possibili soprannomi per un boss.
        """
        if(tipologia == 0):
            if(sesso == "masch"):
                soprannome = ["il battagliero", "il malevolo", "l'usurpatore", "l'anti-eroe", "il divoratore di anime", "il tentatore", "il creatore di vedove"]
            else:
                soprannome = ["la battagliera", "la vedova", "dagli occhi di giada", "l'usurpatrice", "l'ammaliatrice", "la tentatrice", "la macellaia"]
        else:
            soprannome = ["il selvaggio", "il crudele", "il divoratore di anime"]
        return soprannome
